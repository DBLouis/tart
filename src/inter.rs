use crate::math::Vector;
use crate::object::Object;

pub struct Intersection<'a> {
    pub distance: f32,
    pub hit: Vector,
    pub normal: Vector,
    pub view_dir: Vector,
    pub object: &'a Object<'a>,
}

impl<'a> Intersection<'a> {
    pub fn new(
        distance: f32,
        hit: Vector,
        normal: Vector,
        view_dir: Vector,
        object: &'a Object<'a>,
    ) -> Intersection<'a> {
        Intersection {
            distance,
            hit,
            normal,
            view_dir,
            object,
        }
    }
}
