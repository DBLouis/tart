use std::collections::HashMap;
use std::fmt;

use serde::de::{self, DeserializeSeed, MapAccess, SeqAccess, Visitor};
use serde::{Deserialize, Deserializer};

use crate::camera::Camera;
use crate::color::Color;
use crate::inter::Intersection;
use crate::material::{Material, IOR_AIR};
use crate::math::Vector;
use crate::object::{Object, Shape};
use crate::ray::Ray;

#[derive(Clone, Debug)]
pub struct Scene<'a> {
    pub sky: Color,
    pub ior: f32,
    pub camera: Camera,
    pub objects: Vec<Object<'a>>,
}

impl<'a> Scene<'a> {
    // Hit the scene with a ray
    pub fn hit(&self, mut ray: Ray) -> Option<Intersection> {
        let mut hit_object = None;
        for object in &self.objects {
            if object.intersect(&mut ray) {
                hit_object = Some(object);
            }
        }
        hit_object.and_then(|object| {
            let hit = ray.at(ray.distance_max);
            let normal = object.normal_at(hit);
            Some(Intersection::new(
                ray.distance_max,
                hit,
                normal,
                -ray.direction,
                object,
            ))
        })
    }
}

type MaterialMap = HashMap<String, Material>;

impl<'a> Scene<'a> {
    pub fn from_str(
        data: &'a str,
        materials: &'a MaterialMap,
    ) -> Result<Scene<'a>, ron::de::Error> {
        let mut deserializer = ron::de::Deserializer::from_str(data)?;
        const FIELDS: &[&str] = &["sky", "ior", "camera", "objects"];
        let visitor = SceneVisitor { materials };
        deserializer.deserialize_struct("Scene", FIELDS, visitor)
    }
}

#[derive(Deserialize)]
#[serde(field_identifier, rename_all = "lowercase")]
enum SceneField {
    Sky,
    Ior,
    Camera,
    Objects,
}

struct SceneVisitor<'a> {
    materials: &'a MaterialMap,
}

impl<'de, 'a> Visitor<'de> for SceneVisitor<'a> {
    type Value = Scene<'a>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a scene")
    }

    fn visit_map<V>(self, mut map: V) -> Result<Self::Value, V::Error>
    where
        V: MapAccess<'de>,
    {
        let mut sky = None;
        let mut ior = None;
        let mut camera = None;
        let mut objects = None;
        while let Some(key) = map.next_key()? {
            match key {
                SceneField::Sky => {
                    if sky.is_some() {
                        return Err(de::Error::duplicate_field("sky"));
                    }
                    sky = Some(map.next_value()?);
                }
                SceneField::Ior => {
                    if ior.is_some() {
                        return Err(de::Error::duplicate_field("ior"));
                    }
                    ior = Some(map.next_value()?);
                }
                SceneField::Camera => {
                    if camera.is_some() {
                        return Err(de::Error::duplicate_field("camera"));
                    }
                    camera = Some(map.next_value()?);
                }
                SceneField::Objects => {
                    if objects.is_some() {
                        return Err(de::Error::duplicate_field("objects"));
                    }
                    let seed = ObjectVecSeed {
                        materials: self.materials,
                    };
                    objects = Some(map.next_value_seed(seed)?);
                }
            }
        }
        let sky = sky.ok_or_else(|| de::Error::missing_field("sky"))?;
        let ior = ior.unwrap_or(IOR_AIR);
        let camera = camera.ok_or_else(|| de::Error::missing_field("camera"))?;
        let objects = objects.ok_or_else(|| de::Error::missing_field("objects"))?;
        Ok(Scene {
            sky,
            ior,
            camera,
            objects,
        })
    }
}

struct ObjectVecSeed<'a> {
    materials: &'a MaterialMap,
}

impl<'de, 'a> DeserializeSeed<'de> for ObjectVecSeed<'a> {
    type Value = Vec<Object<'a>>;

    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ObjectVecVisitor<'a> {
            materials: &'a MaterialMap,
        }

        impl<'de, 'a> Visitor<'de> for ObjectVecVisitor<'a> {
            type Value = Vec<Object<'a>>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                write!(formatter, "an array of object")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let mut objects = Vec::new();
                loop {
                    let seed = ObjectSeed {
                        materials: self.materials,
                    };
                    match seq.next_element_seed(seed)? {
                        Some(object) => objects.push(object),
                        None => break,
                    };
                }
                Ok(objects)
            }
        }

        let visitor = ObjectVecVisitor {
            materials: self.materials,
        };
        deserializer.deserialize_seq(visitor)
    }
}

struct ObjectSeed<'a> {
    materials: &'a MaterialMap,
}

impl<'de, 'a> DeserializeSeed<'de> for ObjectSeed<'a> {
    type Value = Object<'a>;

    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct ObjectInput {
            shape: Shape,
            material: String,
        }

        let input = ObjectInput::deserialize(deserializer)?;
        let material = {
            let err = || de::Error::custom(format!("material {} not found", input.material));
            self.materials.get(&input.material).ok_or_else(err)?
        };
        Ok(Object {
            shape: input.shape,
            material,
        })
    }
}

impl<'de> Deserialize<'de> for Shape {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        enum ShapeInput {
            Sphere { center: Vector, radius: f32 },
            Plane { normal: Vector, distance: f32 },
            Triangle { a: Vector, b: Vector, c: Vector },
        }

        let input = ShapeInput::deserialize(deserializer)?;
        let shape = match input {
            ShapeInput::Sphere { center, radius } => Shape::Sphere { center, radius },
            ShapeInput::Plane { normal, distance } => {
                let normal = normal.normalized();
                Shape::Plane {
                    position: normal * -distance,
                    normal,
                }
            }
            ShapeInput::Triangle { a, b, c } => {
                let normal = ((b - a).cross(c - a)).normalized();
                Shape::Triangle { a, b, c, normal }
            }
        };
        Ok(shape)
    }
}
