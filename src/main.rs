use std::fs::{self, File};
use std::path::Path;
use std::process;

use clap::{load_yaml, App};
use openexr;

use tart::{renderer, scene::Scene};

const DEFAULT_SAMPLES: usize = 1024;

fn main() {
    let cli = load_yaml!("cli.yml");
    let matches = App::from_yaml(cli).get_matches();

    let materials_path = matches.value_of("materials").unwrap_or_else(|| {
        eprintln!("Materials file path missing");
        process::exit(1);
    });
    let scene_path = matches.value_of("scene").unwrap_or_else(|| {
        eprintln!("Scene file path missing");
        process::exit(1);
    });
    let output_path = matches.value_of("output").unwrap_or_else(|| {
        eprintln!("Output file path missing");
        process::exit(1);
    });
    let samples = matches
        .value_of("samples")
        .map(str::parse::<usize>)
        .transpose()
        .unwrap_or_else(|_| {
            eprintln!("Samples must be an positive integer");
            process::exit(1);
        })
        .unwrap_or(DEFAULT_SAMPLES);

    let materials = fs::read_to_string(materials_path).unwrap_or_else(|err| {
        eprintln!("Failed to load materials file: {}", err);
        process::exit(2);
    });
    let materials = ron::de::from_str(&materials).unwrap_or_else(|err| {
        eprintln!("Failed to load materials: {}", err);
        process::exit(2);
    });

    let scene = fs::read_to_string(scene_path).unwrap_or_else(|err| {
        eprintln!("Failed to load scene file: {}", err);
        process::exit(3);
    });
    let scene = Scene::from_str(&scene, &materials).unwrap_or_else(|err| {
        eprintln!("Failed to load scene: {}", err);
        process::exit(3);
    });

    if Path::new(&output_path).exists() {
        eprintln!("Image file already exist");
        process::exit(4);
    }

    let mut file = File::create(&output_path).unwrap_or_else(|err| {
        eprintln!("Failed to open file: {}", err);
        process::exit(5);
    });

    let pixels = renderer::render(&scene, samples);

    let mut output_file = openexr::ScanlineOutputFile::new(
        &mut file,
        openexr::Header::new()
            .set_compression(openexr::header::Compression::ZIP_COMPRESSION)
            .set_resolution(scene.camera.res.width, scene.camera.res.height)
            .add_channel("R", openexr::PixelType::FLOAT)
            .add_channel("G", openexr::PixelType::FLOAT)
            .add_channel("B", openexr::PixelType::FLOAT),
    )
    .unwrap();

    let mut frame = openexr::FrameBuffer::new(scene.camera.res.width, scene.camera.res.height);
    frame.insert_channels(&["R", "G", "B"], &pixels);

    output_file.write_pixels(&frame).unwrap();
}
