pub mod camera;
pub mod inter;
pub mod material;
pub mod object;
pub mod ray;
pub mod renderer;
pub mod scene;

pub mod math {
    pub type Vector = vek::vec::repr_c::vec3::Vec3<f32>;
    pub type Quaternion = vek::quaternion::repr_c::Quaternion<f32>;
}

pub mod color {
    pub type Color = vek::vec::repr_c::rgb::Rgb<f32>;
}
