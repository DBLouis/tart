use std::{
    io::{self, Write},
    sync::atomic::{AtomicU32, Ordering},
    time::Instant,
};

use rand::prelude::*;
use rand_xoshiro::Xoshiro256Plus;
use rayon::{prelude::*, ThreadPoolBuilder};
use vek::ops::Clamp;

use crate::{
    color::Color,
    material::Material,
    math::{Quaternion, Vector},
    ray::Ray,
    scene::Scene,
};

const NUM_THREADS: usize = 8;
//const IMPORTANCE_THRESHOLD: f32 = 0.01;
const BOUNCE_LIMIT: usize = 8;

type Prng = Xoshiro256Plus;

// Render a scene into an image
pub fn render(scene: &Scene, samples: usize) -> Vec<(f32, f32, f32)> {
    let ratio = 1.0 / scene.camera.res.ratio();
    let width = scene.camera.res.width;
    let height = scene.camera.res.height;

    let dx = (2.0 / width as f32) * scene.camera.xdir;
    let dy = (2.0 / height as f32) * ratio * -scene.camera.ydir;

    let corner = scene.camera.center - scene.camera.xdir + (scene.camera.ydir * ratio);

    println!("Dimensions: {}x{}", width, height);
    println!("Samples per pixel: {}", samples);

    let start = Instant::now();
    let progress = AtomicU32::new(0);

    ThreadPoolBuilder::new()
        .num_threads(NUM_THREADS)
        .build_global()
        .unwrap();

    let mut pixels = vec![(0.0, 0.0, 0.0); (width * height) as usize];

    #[cfg(debug_assertions)]
    let iter = pixels.par_chunks_mut(width as usize);
    #[cfg(not(debug_assertions))]
    let iter = pixels.par_chunks_mut(width as usize).panic_fuse();

    iter.enumerate().for_each(|(y, row)| {
        let mut rng = Prng::from_entropy();

        row.iter_mut().enumerate().for_each(|(x, p)| {
            let mut color = Color::black();

            for _ in 0..samples {
                let rx: f32 = rng.gen();
                let ry: f32 = rng.gen();
                let target = corner + ((x as f32 + rx) * dx) + ((y as f32 + ry) * dy);
                let ray = Ray::between(scene.camera.eye, target);
                let s = throw_ray(&mut rng, scene, ray);
                assert!(s.r >= 0.0 && s.g >= 0.0 && s.b >= 0.0);
                color += s;
            }

            p.0 = color.r / samples as f32;
            p.1 = color.g / samples as f32;
            p.2 = color.b / samples as f32;
        });

        let rows = progress.fetch_add(1, Ordering::Relaxed);

        if rows % NUM_THREADS as u32 == 0 {
            print!("\r{:.2}%", (rows as f32 / height as f32) * 100.0);
            io::stdout().flush().unwrap();
        }
    });

    println!("\r100.0%");
    println!("Took {} seconds", start.elapsed().as_secs());

    pixels
}

fn throw_ray(rng: &mut Prng, scene: &Scene, mut ray: Ray) -> Color {
    let mut color = Color::black();
    let mut importance = Color::white();

    let mut sky = true;
    let mut bounces = 0usize;

    while let Some(inter) = scene.hit(ray) {
        match *inter.object.material {
            Material::Light { emittance } => {
                color += importance * emittance;
                sky = false;
                break;
            }
            Material::Conductor => {}
            Material::Dielectric {
                ior,
                roughness,
                diffuse_color,
                specular_color,
            } => {
                let normal = inter.normal.normalized();
                let view_dir = inter.view_dir.normalized();

                let alpha = roughness * roughness;

                let to_world =
                    Quaternion::rotation_from_to_3d(Vector::unit_z(), normal).normalized();

                let r1 = rng.gen();
                let r2 = rng.gen();

                //let (sample, pdf) = ggx_sample(alpha, r1, r2);
                let (sample, pdf) = beckmann_sample(alpha, r1, r2);
                //let (sample, pdf) = uniform_sample(r1, r2);
                let halfway = (to_world * sample).normalized();

                let n_v = normal.dot(view_dir);
                let v_h = view_dir.dot(halfway);

                if v_h <= 0.0 || n_v <= 0.0 {
                    sky = false;
                    break;
                }

                let light_dir = (-view_dir).reflected(halfway).normalized();

                let n_h = normal.dot(halfway);
                let h_l = halfway.dot(light_dir);
                let n_l = normal.dot(light_dir);

                debug_assert!(h_l >= 0.0);
                debug_assert!(n_h >= 0.0);

                if n_l <= 0.0 {
                    sky = false;
                    break;
                }

                let d = beckmann(alpha, n_h);
                //let d = ggx(alpha, n_h);
                let f = fresnel_schlick(h_l, scene.ior, ior);
                let g = smith_beckmann_schlick(alpha, n_l, n_v);
                //let g = smith_ggx(alpha, n_l, n_v);

                debug_assert!(!d.is_nan() && d.is_finite());
                debug_assert!(!f.is_nan() && f.is_finite());
                debug_assert!(!g.is_nan() && g.is_finite());

                let cts = specular_color * ((d * f * g) / (4.0 * n_v * n_l));
                let ctd = diffuse_color * (1.0 - f);

                let brdf = Clamp::<f32>::clamp01(cts + ctd);
                //let brdf = Color::partial_max(Color::black(), cts + ctd);

                debug_assert!(brdf.r >= 0.0 && brdf.g >= 0.0 && brdf.b >= 0.0);

                //let pdf = 1.0 / pdf;

                let pdf = (pdf * n_h) / (4.0 * v_h);
                //let pdf = pdf / (4.0 * v_h);

                debug_assert!(!pdf.is_nan() && pdf.is_finite());
                debug_assert!(pdf >= 0.0);

                //importance *= brdf / pdf;
                importance *= brdf * pdf;

                ray = ray.bounce(inter.hit, light_dir);
            }
        }
        if bounces >= BOUNCE_LIMIT {
            break;
        }
        bounces += 1;
        //if importance.r < IMPORTANCE_THRESHOLD
        //    && importance.g < IMPORTANCE_THRESHOLD
        //    && importance.b < IMPORTANCE_THRESHOLD
        //{
        //    break;
        //}
    }

    if sky {
        color += importance * scene.sky;
    }

    color
}

fn uniform_sample(r1: f32, r2: f32) -> (Vector, f32) {
    let phi = 2.0 * std::f32::consts::PI * r1;
    let sin_theta = (1.0 - r2 * r2).sqrt();

    let x = sin_theta * phi.cos();
    let y = sin_theta * phi.sin();
    let z = r2;

    let sample = Vector::new(x, y, z);

    (sample, 2.0 * std::f32::consts::PI)
}

fn ggx_sample(alpha: f32, r1: f32, r2: f32) -> (Vector, f32) {
    let alpha_1 = alpha - 1.0;

    let theta = ((1.0 - r1) / (alpha_1 * r1 + 1.0)).sqrt().acos();
    let phi = 2.0 * std::f32::consts::PI * r2;

    let x = theta.sin() * phi.cos();
    let y = theta.sin() * phi.sin();
    let z = theta.cos();

    let sample = Vector::new(x, y, z);

    let exp = alpha_1 * theta.cos() * theta.cos() + 1.0;
    let pdf = alpha / (std::f32::consts::PI * exp * exp);

    (sample, pdf)
}

fn beckmann_sample(alpha: f32, r1: f32, r2: f32) -> (Vector, f32) {
    let phi = 2.0 * std::f32::consts::PI * r1;
    let tan_theta = (-alpha * (1.0 - r2).ln()).sqrt();
    let theta = tan_theta.atan();

    let x = theta.sin() * phi.cos();
    let y = theta.sin() * phi.sin();
    let z = theta.cos();

    let sample = Vector::new(x, y, z);

    let cos3_theta = theta.cos() * theta.cos() * theta.cos();
    let tan2_theta = tan_theta * tan_theta;
    let pdf = (2.0 * theta.sin() / (alpha * cos3_theta)) * (-tan2_theta / alpha).exp();

    (sample, pdf)
}

fn ggx(alpha: f32, n_h: f32) -> f32 {
    let cos_2 = n_h * n_h;
    let t = (alpha - 1.0) * cos_2 + 1.0;
    alpha / (std::f32::consts::PI * t * t)
}

fn beckmann(alpha: f32, n_h: f32) -> f32 {
    if n_h <= 0.0 {
        return 0.0;
    }
    let cos_2 = n_h * n_h;
    let tan_2 = (1.0 - cos_2) / cos_2;
    let cos_4 = cos_2 * cos_2;
    (-tan_2 / alpha).exp() / (std::f32::consts::PI * alpha * cos_4)
}

fn fresnel_schlick(h_l: f32, ior_in: f32, ior_out: f32) -> f32 {
    let ior_ratio = (ior_in - ior_out) / (ior_in + ior_out);
    let r0 = ior_ratio * ior_ratio;
    let c = 1.0 - h_l;
    r0 + (1.0 - r0) * c * c * c * c * c
}

fn smith_ggx(alpha: f32, n_l: f32, n_v: f32) -> f32 {
    let a = n_v * (alpha + (1.0 - alpha) * n_l * n_l).sqrt();
    let b = n_l * (alpha + (1.0 - alpha) * n_v * n_v).sqrt();
    (2.0 * n_l * n_v) / (a + b)
}

fn smith_beckmann_schlick(alpha: f32, n_l: f32, n_v: f32) -> f32 {
    fn smith_g1_beckmann_schlick(alpha: f32, n_d: f32) -> f32 {
        let k = alpha * (2.0 / std::f32::consts::PI).sqrt();
        n_d / (n_d * (1.0 - k) + k)
    }
    smith_g1_beckmann_schlick(alpha, n_l) * smith_g1_beckmann_schlick(alpha, n_v)
}
