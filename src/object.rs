use crate::material::Material;
use crate::math::Vector;
use crate::ray::Ray;

#[derive(Clone, Copy, Debug)]
pub struct Object<'a> {
    pub shape: Shape,
    pub material: &'a Material,
}

#[derive(Clone, Copy, Debug)]
pub enum Shape {
    Sphere {
        center: Vector,
        radius: f32,
    },
    Plane {
        position: Vector,
        normal: Vector,
    },
    Triangle {
        a: Vector,
        b: Vector,
        c: Vector,
        normal: Vector,
    },
}

impl<'a> Object<'a> {
    pub fn intersect(self, ray: &mut Ray) -> bool {
        match self.shape {
            Shape::Sphere { center, radius } => intersect_sphere(ray, center, radius),
            Shape::Plane { position, normal } => intersect_plane(ray, position, normal),
            Shape::Triangle { a, b, c, .. } => intersect_triangle(ray, a, b, c),
        }
    }
    pub fn normal_at(self, position: Vector) -> Vector {
        match self.shape {
            Shape::Sphere { center, .. } => (position - center).normalized(),
            Shape::Plane { normal, .. } => normal,
            Shape::Triangle { normal, .. } => normal,
        }
    }
}

fn intersect_sphere(ray: &mut Ray, center: Vector, radius: f32) -> bool {
    let (x0, x1) = {
        let l = ray.position - center;
        let a = ray.direction.dot(ray.direction);
        let b = 2.0 * l.dot(ray.direction);
        let c = l.dot(l) - (radius * radius);
        let delta = b * b - 4.0 * a * c;
        if delta <= 0.0 {
            return false;
        }
        let q = if b > 0.0 {
            -0.5 * (b + delta.sqrt())
        } else {
            -0.5 * (b - delta.sqrt())
        };
        (q / a, c / q)
    };
    // Order distances
    let (x0, x1) = if x0 > x1 { (x1, x0) } else { (x0, x1) };
    let x = if x0 <= ray.distance_min { x1 } else { x0 };
    if x > ray.distance_min && x < ray.distance_max {
        ray.distance_max = x;
        true
    } else {
        false
    }
}

const EPSILON: f32 = 1E-6;

fn intersect_plane(ray: &mut Ray, position: Vector, normal: Vector) -> bool {
    let denominator = normal.dot(-ray.direction);
    if denominator <= EPSILON {
        return false;
    }
    let x = (ray.position - position).dot(normal) / denominator;
    if x > ray.distance_min && x < ray.distance_max {
        ray.distance_max = x;
        true
    } else {
        false
    }
}

fn intersect_triangle(ray: &mut Ray, a: Vector, b: Vector, c: Vector) -> bool {
    let e0 = b - a;
    let e1 = c - a;
    let pvec = ray.direction.cross(e1);
    let delta = e0.dot(pvec);
    if delta > -EPSILON && delta < EPSILON {
        return false;
    }
    let idelta = 1.0 / delta;
    let tvec = ray.position - a;
    let u = tvec.dot(pvec) * idelta;
    if u < 0.0 || u > 1.0 {
        return false;
    }
    let qvec = tvec.cross(e0);
    let v = ray.direction.dot(qvec) * idelta;
    if v < 0.0 || u + v > 1.0 {
        return false;
    }
    let x = e1.dot(qvec) * idelta;
    if x > ray.distance_min && x < ray.distance_max {
        ray.distance_max = x;
        true
    } else {
        false
    }
}
