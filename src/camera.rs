use serde::{Deserialize, Deserializer};

use crate::math::Vector;

#[derive(Clone, Copy, Debug, Deserialize)]
pub struct Resolution {
    pub width: u32,
    pub height: u32,
}

impl Resolution {
    pub fn ratio(&self) -> f32 {
        self.width as f32 / self.height as f32
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Camera {
    pub eye: Vector,
    pub target: Vector,
    pub up: Vector,
    pub res: Resolution,
    pub fov: f32,
    pub gamma: f32,
    pub exposure: f32,
    pub center: Vector,
    pub xdir: Vector,
    pub ydir: Vector,
    pub zdir: Vector,
}

pub const DEFAULT_GAMMA: f32 = 2.2;
pub const DEFAULT_EXPOSURE: f32 = 0.5;

impl Camera {
    pub fn new(
        eye: Vector,
        target: Vector,
        up: Vector,
        res: Resolution,
        fov: f32,
        gamma: f32,
        exposure: f32,
    ) -> Camera {
        let zdir = (target - eye).normalized();
        let xdir = up.cross(zdir).normalized();
        let ydir = zdir.cross(xdir).normalized();
        let center = eye + (1.0 / ((fov / 360.0 * std::f32::consts::PI).tan()) * zdir);
        Camera {
            eye,
            target,
            up,
            fov,
            gamma,
            exposure,
            res,
            center,
            xdir,
            ydir,
            zdir,
        }
    }
}

impl<'de> Deserialize<'de> for Camera {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct CameraInput {
            eye: Vector,
            target: Vector,
            up: Vector,
            res: Resolution,
            fov: f32,
            #[serde(default = "default_gamma")]
            gamma: f32,
            #[serde(default = "default_exposure")]
            exposure: f32,
        }

        fn default_gamma() -> f32 {
            DEFAULT_GAMMA
        }

        fn default_exposure() -> f32 {
            DEFAULT_EXPOSURE
        }

        let input = CameraInput::deserialize(deserializer)?;
        Ok(Camera::new(
            input.eye,
            input.target,
            input.up,
            input.res,
            input.fov,
            input.gamma,
            input.exposure,
        ))
    }
}
