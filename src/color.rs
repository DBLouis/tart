use std::fmt;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign};

use serde::de::{self, SeqAccess, Visitor};
use serde::{Deserialize, Deserializer};

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

pub const WHITE: Color = Color {
    r: 1.0,
    g: 1.0,
    b: 1.0,
};
pub const GRAY: Color = Color {
    r: 0.5,
    g: 0.5,
    b: 0.5,
};
pub const BLACK: Color = Color {
    r: 0.0,
    g: 0.0,
    b: 0.0,
};
pub const RED: Color = Color {
    r: 1.0,
    g: 0.0,
    b: 0.0,
};
pub const GREEN: Color = Color {
    r: 0.0,
    g: 1.0,
    b: 0.0,
};
pub const BLUE: Color = Color {
    r: 0.0,
    g: 0.0,
    b: 1.0,
};

impl Color {
    pub fn new(r: f64, g: f64, b: f64) -> Color {
        Color { r, g, b }
    }
    pub fn correct_gamma(self, gamma: f64) -> Color {
        let gamma = 1.0 / gamma;
        Color {
            r: self.r.powf(gamma),
            g: self.g.powf(gamma),
            b: self.b.powf(gamma),
        }
    }
    pub fn tone_map(self, exposure: f64) -> Color {
        Color {
            r: 1.0 - (-self.r * exposure).exp(),
            g: 1.0 - (-self.g * exposure).exp(),
            b: 1.0 - (-self.b * exposure).exp(),
        }
    }
    pub fn clamp(self) -> Color {
        Color {
            r: self.r.max(0.0).min(1.0),
            g: self.g.max(0.0).min(1.0),
            b: self.b.max(0.0).min(1.0),
        }
    }
}

impl Add for Color {
    type Output = Color;

    fn add(self, rhs: Color) -> Color {
        Color {
            r: self.r + rhs.r,
            g: self.g + rhs.g,
            b: self.b + rhs.b,
        }
    }
}

impl AddAssign for Color {
    fn add_assign(&mut self, rhs: Color) {
        *self = *self + rhs;
    }
}

impl Div<f64> for Color {
    type Output = Color;

    fn div(self, rhs: f64) -> Color {
        debug_assert!(rhs != 0.0);
        Color {
            r: self.r / rhs,
            g: self.g / rhs,
            b: self.b / rhs,
        }
    }
}

impl DivAssign<f64> for Color {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs;
    }
}

impl Mul<f64> for Color {
    type Output = Color;

    fn mul(self, rhs: f64) -> Color {
        Color {
            r: self.r * rhs,
            g: self.g * rhs,
            b: self.b * rhs,
        }
    }
}

impl Mul<Color> for f64 {
    type Output = Color;

    fn mul(self, rhs: Color) -> Color {
        rhs * self
    }
}

impl Mul for Color {
    type Output = Color;

    fn mul(self, rhs: Color) -> Color {
        Color {
            r: self.r * rhs.r,
            g: self.g * rhs.g,
            b: self.b * rhs.b,
        }
    }
}

impl MulAssign<f64> for Color {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs;
    }
}

impl MulAssign<Color> for Color {
    fn mul_assign(&mut self, rhs: Color) {
        *self = *self * rhs;
    }
}

// Custom deserializer for Color
impl<'de> Deserialize<'de> for Color {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct ColorVisitor;

        impl<'de> Visitor<'de> for ColorVisitor {
            type Value = Color;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a color by name or RGB components")
            }

            fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let r = seq.next_element()?;
                let g = seq.next_element()?;
                let b = seq.next_element()?;
                let r = r.ok_or_else(|| de::Error::missing_field("r"))?;
                let g = g.ok_or_else(|| de::Error::missing_field("g"))?;
                let b = b.ok_or_else(|| de::Error::missing_field("b"))?;
                Ok(Color::new(r, g, b))
            }

            fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                match value {
                    "white" => Ok(WHITE),
                    "black" => Ok(BLACK),
                    "red" => Ok(RED),
                    "green" => Ok(GREEN),
                    "blue" => Ok(BLUE),
                    _ => Err(de::Error::custom("unknown color name")),
                }
            }
        }

        deserializer.deserialize_any(ColorVisitor)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const A: Color = Color {
        r: 0.48,
        g: 0.1516,
        b: 0.2342,
    };
    const B: Color = Color {
        r: 0.2432,
        g: 0.615,
        b: 0.184,
    };

    #[test]
    fn add_color() {
        const R: Color = Color {
            r: 0.7232,
            g: 0.7666,
            b: 0.4182,
        };
        assert_eq!(A + B, R);
    }

    #[test]
    fn div() {
        const R: Color = Color {
            r: 0.24,
            g: 0.0758,
            b: 0.1171,
        };
        assert_eq!(A / 2.0, R);
    }

    #[test]
    fn mul() {
        const R: Color = Color {
            r: 0.4864,
            g: 1.23,
            b: 0.368,
        };
        assert_eq!(2.0 * B, R);
    }

    #[test]
    fn mul_color() {
        const R: Color = Color {
            r: 0.48,
            g: 0.0,
            b: 0.0,
        };
        assert_eq!(A * RED, R);
    }

    #[test]
    #[should_panic]
    fn invalid_name() {
        let data = r#""invalid""#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    fn valid_name() {
        let data = r#""blue""#;
        let color: Color = ron::de::from_str(data).unwrap();
        assert_eq!(color, BLUE);
    }

    #[test]
    #[should_panic]
    fn negative_red() {
        let data = r#"[ -0.48, 0.1516, 0.2342 ]"#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    #[should_panic]
    fn negative_green() {
        let data = r#"[ 0.48, -0.1516, 0.2342 ]"#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    #[should_panic]
    fn negative_blue() {
        let data = r#"[ 0.48, 0.1516, -0.2342 ]"#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    #[should_panic]
    fn too_much_red() {
        let data = r#"[ 4.8, 0.1516, 0.2342 ]"#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    #[should_panic]
    fn too_much_green() {
        let data = r#"[ 0.48, 1.516, 0.2342 ]"#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    #[should_panic]
    fn too_much_blue() {
        let data = r#"[ 0.48, 0.1516, 2.342 ]"#;
        let _: Color = ron::de::from_str(data).unwrap();
    }

    #[test]
    fn valid_components() {
        let data = r#"[ 0.48, 0.1516, 0.2342 ]"#;
        let color: Color = ron::de::from_str(data).unwrap();
        assert_eq!(color, A);
    }
}
