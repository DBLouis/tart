use serde::de;
use serde::{Deserialize, Deserializer};

use crate::color::Color;

pub const IOR_AIR: f32 = 1.000_272;

#[derive(Clone, Copy, Debug, Deserialize)]
pub enum Material {
    Light {
        emittance: Color,
    },
    Conductor,
    Dielectric {
        #[serde(deserialize_with = "deserialize_ior")]
        ior: f32,
        #[serde(deserialize_with = "deserialize_roughness")]
        roughness: f32,
        diffuse_color: Color,
        specular_color: Color,
    },
}

fn deserialize_ior<'de, D>(deserializer: D) -> Result<f32, D::Error>
where
    D: Deserializer<'de>,
{
    let ior = f32::deserialize(deserializer)?;
    if ior < 1.0 {
        return Err(de::Error::custom(
            "index of refraction must be superior to 1.0",
        ));
    }
    Ok(ior)
}

fn deserialize_roughness<'de, D>(deserializer: D) -> Result<f32, D::Error>
where
    D: Deserializer<'de>,
{
    let roughness = f32::deserialize(deserializer)?;
    if roughness < 0.0 {
        return Err(de::Error::custom("roughness cannot be negative"));
    }
    if roughness > 1.0 {
        return Err(de::Error::custom("roughness cannot be superior to 1.0"));
    }
    Ok(roughness)
}

impl Material {
    pub fn new_dielectric(
        ior: f32,
        roughness: f32,
        diffuse_color: Color,
        specular_color: Color,
    ) -> Material {
        debug_assert!(ior >= 1.0);
        debug_assert!(roughness >= 0.0 && roughness <= 1.0);
        Material::Dielectric {
            ior,
            roughness,
            diffuse_color,
            specular_color,
        }
    }
}
