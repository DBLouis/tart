use crate::math::Vector;

const ACNE_BIAS: f32 = 1E-4;

#[derive(Clone, Copy, Debug)]
pub struct Ray {
    pub position: Vector,
    pub direction: Vector,
    pub distance_min: f32,
    pub distance_max: f32,
    pub depth: usize,
}

impl Ray {
    pub fn new(position: Vector, direction: Vector) -> Ray {
        Ray {
            position,
            direction: direction.normalized(),
            distance_min: ACNE_BIAS,
            distance_max: std::f32::INFINITY,
            depth: 0,
        }
    }
    pub fn bounce(&self, position: Vector, direction: Vector) -> Ray {
        Ray {
            position,
            direction,
            distance_min: ACNE_BIAS,
            distance_max: std::f32::INFINITY,
            depth: self.depth + 1,
        }
    }
    pub fn between(from: Vector, to: Vector) -> Ray {
        Ray::new(from, to - from)
    }
    pub fn at(&self, distance: f32) -> Vector {
        self.position + (distance * self.direction)
    }
}
